<?php

require_once 'TimeToWordConvertingInterface.php';

class TimeToWordConverting implements TimeToWordConvertingInterface
{
    private $minuteDict = array(
        1 => "одна",
        2  => "две",
    );

    private $generalDict = array(
        0 => "ноль",
        1 => "один",
        2  => "два",
        3  => "три",
        4 => "четыре",
        5 => "пять",
        6 => "шесть",
        7 => "семь",
        8 => "восемь",
        9 => "девять",
        10 => "десять",
        11 => "одиннадцать",
        12 => "двенадцать",
        13 => "тринадцать",
        14 => "четырнадцать",
        15 => "пятнадцать",
        16 => "шестнадцать",
        17 => "семнадцать",
        18 => "восемнадцать",
        19 => "девятнадцать",
        20 => "двадцать",
    );

    private $quarterHalfDict = array(
        1 => "первого",
        2  => "второго",
        3  => "третьего",
        4 => "четвертого",
        5 => "пятого",
        6 => "шестого",
        7 => "седьмого",
        8 => "восьмого",
        9 => "девятого",
        10 => "десятого",
        11 => "одиннадцатого",
        12 => "двеннадцатого",
    );

    private $afterBeforeDict = array(
        1 => "одного",
        2  => "двух",
        3  => "трёх",
        4 => "четырех",
        5 => "пяти",
        6 => "шести",
        7 => "семи",
        8 => "восьми",
        9 => "девяти",
        10 => "десяти",
        11 => "одиннадцати",
        12 => "двеннадцати",
    );

    public function convert(int $hours, int $minutes): string
    {
        if($hours < 1 || $hours > 12 || $minutes < 0 || $minutes > 59){
            exit("Введеные некорректные данные.");
        }

        if($minutes === 0){
            return $this->formatOClock($hours, $minutes);
        }
        elseif($minutes === 15 || $minutes === 30){
            return $this->formatQuarterPastAndHalf($hours, $minutes);
        }
        elseif($minutes === 45){
            return $this->formatQuarterTo($hours, $minutes);
        }
        else{
            return $this->formatAfterBefore($hours, $minutes);
        }
    }

    private function formatOClock(int $hours, int $minutes): string
    {
        $result = $this->formatDeclinationForNumber($hours, $this->generalDict) . " " . $this->descriptiveWordConverter($hours % 20, true);
        return $this->format($hours, $minutes, $result);
    }

    private function formatQuarterPastAndHalf(int $hours, int $minutes): string
    {
        $result = $minutes === 15 ? "Четверть " : "Половина ";
        $result = $result . $this->formatDeclinationForNumber($hours + 1, $this->quarterHalfDict);
        return $this->format($hours, $minutes, $result);
    }

    private function formatQuarterTo(int $hours, int $minutes): string
    {
        $result = "Без четверти ";
        $result = $result . $this->formatDeclinationForNumber($hours + 1, $this->generalDict);
        return $this->format($hours, $minutes, $result);
    }

    private function formatAfterBefore(int $hours, int $minutes): string
    {
        $pretext = " после ";
        $initiallyMinutes = $minutes;
        $viewHour = $hours;
        if($minutes > 30){
            $pretext = " до ";
            $minutes = 60 - $minutes;
            $viewHour++;
        }
        $result = $this->formatDeclinationForMinute($minutes, $this->generalDict, $this->generalDict) . " " . $this->descriptiveWordConverter($minutes % 20, false) . $pretext . $this->formatDeclinationForNumber($viewHour, $this->afterBeforeDict);
        return $this->format($hours, $initiallyMinutes, $result);
    }

    private function format(int $hours, int $minutes, string $result): string
    {
        $minutes = intdiv($minutes, 10) > 0 ? $minutes : "0" . $minutes;
        return $hours . ":" . $minutes . " - " . mb_convert_case(mb_substr($result, 0, 1), MB_CASE_UPPER, 'UTF-8') . mb_substr($result, 1, strlen($result) - 1) . ".";
    }

    public function formatDeclinationForNumber(int $value, array $dictForLastNumber): string
    {
        return $value / 12 > 1 ? $dictForLastNumber[1] : $dictForLastNumber[$value];
    }

    public function formatDeclinationForMinute(int $value, array $dict, array $dictForFirstNumber): string
    {
        $result = "";
        if($value / 20 > 1){
            $result = $result . $dictForFirstNumber[$value - $value % 10] . " ";
        }
        $value = $value % 20;
        return $result . ($value === 1 || $value === 2 ? $this->minuteDict[$value] : $dict[$value === 0 ? 20 : $value]);
    }

    private function descriptiveWordConverter(int $value, bool $isHour): string
    {
        if($value === 1){
            return $isHour ? "час" : "минута";
        }
        elseif($value >= 2 && $value <= 4){
            return $isHour ? "часа" : "минуты";
        }
        else{
            return $isHour ? "часов" : "минут";
        }
    }
}